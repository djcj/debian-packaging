NuGet
=====

Package manager for NuGet repos

NuGet is the package manager for the Microsoft development platform including .NET.
The NuGet client tools provide the ability to produce and consume packages.
The NuGet Gallery is the central package repository used by all package authors and consumers.

Homepage: http://www.nuget.org

