GLEW
====

This will build packages of all GLEW versions (1.0, 1.1, [...]).
This can be useful if games were linked dynamically against certain library versions which aren't available
in the distribution's repositories.

