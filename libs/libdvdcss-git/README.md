libdvdcss
=========

libdvdcss is a simple library designed for accessing DVDs like a
block device without having to bother about the decryption.

You'll need this to watch css encrypted DVDs on your PC.
