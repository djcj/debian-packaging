Pidgin IM
=========

On some ICQ accounts there's an annoying and pointless warning appearing on each startup.
This version of Pidgin will be patched so that the message won't appear anymore.

After building the packages you have to manually install them in this order: `pidgin-data libpurple0 pidgin`

I recommend using gdebi for manual installations.

Homepage: http://www.pidgin.im/

PS: If you're running Ubuntu and Unity integration is not enabled or the package's version does not
begin with `1:`, try to run `make` with `UBUNTU=1`
