QuickHash GUI
=============

A GUI that enables rapid selection & hashing of files (individually or recursively), text and physical disks.
MD5, SHA1, SHA256, SHA512 available. CSV\HTML\Clipboard output.

**Homepage:** http://quickhash.sourceforge.net/

