SteamCMD
========

[Steam](http://www.steampowered.com) is a software content delivery system developed by [Valve software](http://www.valvesoftware.com).

The Steam Console Client or SteamCMD is a command-line version of the Steam client.
Its primary use is to install and update various dedicated servers available on Steam
using a command-line interface. It works with games that use the SteamPipe content system.

Homepage: https://developer.valvesoftware.com/wiki/SteamCMD

