Skulltag
========

Skulltag is a port for the original Doom and Doom II by id Software.

Skulltag brings classic Doom into the 21st century, maintaining the essence of
what has made Doom great for so many years and, at the same time, adding new features
to modernize it, creating a fresh, fun new experience.

Sadly, it is no longer being actively developed (you can read more about that [here](http://www.skulltag.com/itends)),
but is still available for single player and archival purposes.

Homepage: http://www.skulltag.com/
