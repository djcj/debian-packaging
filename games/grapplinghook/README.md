Grappling Hook
==============

Grappling Hook is an innovative first-person action puzzle / platform game.
You use the Grappling Hook as a mighty high-tech tool to overcome various challenging
obstacles by performing incredible moves and fast combinations of daring tricks.

The dynamic and surprising levels open opportunities to use the Grappling Hook in
unique ways never seen before in a game. Making your way through the levels requires
both skill and creativity, while the increasing difficulty pushes you to become
a real master of the Grappling Hook.

To build a package of the full version, copy the official tarball of the full version
into this directory and run `make`.
If that tarball is missing the demo version will automatically be downloaded and built.
If you want to built a separate package for the architecture independend game files
run `make COMMON_PKG=1` instead.

http://ghook.speedrungames.com/

