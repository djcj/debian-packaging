Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Grappling Hook
Source: http://ghook.speedrungames.com/

Files: *
Copyright: 2009 Christian Teister
License: EULA
 END USER LICENSE AGREEMENT
 .
 Please be sure to carefully read and understand all of the rights and 
 restrictions described in this End-User License Agreement ("EULA"). This 
 document is an agreement between you and Christian Teister.
 By installing, copying, or otherwise using the software, you agree to be bound 
 by the terms of this License Agreement.
 .
 Ownership:
 This software program (the "Game"), any printed materials, any on-line or 
 electronic documentation, any images, photographs, animations, video, music, 
 text and "applets" incorporated into the Software and any results or proceeds 
 created by you using the Software and all copies and derivative works of such 
 software program and materials are the copyrighted work of Christian Teister or 
 its Licensors.
 .
 Grant of License:
 The Game is licensed and not sold to you and its use is subject to this EULA.
 Christian Teister hereby grants to you a non-exclusive license to use a single 
 copy of the object code version of the Game for your personal, non-commercial 
 home entertainment use on one personal computer or other compatible electronic 
 device.
 .
 Restrictions:
 You may not decompile, modify, reverse engineer, disassemble or otherwise 
 reproduce the Game.
 You may not copy, offer for public display or create derivative works thereof, 
 except to the extent that such restrictions are expressly prohibited by law.
 You may not rent, lease, loan, sublicense or distribute the Game, or offer it 
 on a pay-per-play, coin-op or other for charge (or free) basis.
 You may not use the Game to infringe the copyrights or other intellectual 
 property rights of others in any way.
 You may not delete or obscure any copyright, trademark or other proprietary 
 notice on the Game or accompanying printed materials.
 .
 Disclaimer of Warranty:
 You expressly agree and acknowledge that use of this Software is at your own 
 risk. The Software is provided "as is" without warranty of any kind, either 
 express or implied.
 .
 Limitation of Liability:
 Christian Teister shall not be liable in any way for loss or damage of any kind 
 resulting from the use of the Game, including, but not limited to, loss of 
 goodwill, work stoppage, computer failure or malfunction, or any and all other 
 commercial damages or losses. Some states do not allow the exclusion or 
 limitation of incidental or consequential damages, or allow limitations on how 
 long an implied warranty lasts, so the above limitations may not apply.
 .
 Termination:
 This License Agreement is effective until terminated. You may terminate the 
 License Agreement at any time by uninstalling the Game and destroying all 
 copies of the Game in your possession or control. Christian Teister may, at its 
 discretion, terminate this License Agreement in the event that you fail to 
 comply with the terms and conditions contained herein. Upon termination of this 
 EULA, you agree to immediately uninstall the Game and destroy all copies of the 
 Game.

Files: GrapplingHook_Demo/lib/*lwjgl*
Copyright: 2002-2013 Lightweight Java Game Library Project
License: 3-clause-BSD
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 * Neither the name of 'Light Weight Java Game Library' nor the names of
   its contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: 2014 djcj <djcj@gmx.de>
License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
