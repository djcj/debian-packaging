itch
====

The goal of this project is to give you a desktop application that you can
download and run games from [itch.io](https://itch.io) with.
Additionally you should be able to update games and get notified when games are updated.
The goal is not to replace the itch.io website.
